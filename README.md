# Fluid Numerics OSU benchmark system for Fluid-Slurm-GCP
Copyright 2020 Fluid Numerics LLC


## Getting started

Once you've launched your fluid-slurm-gcp +OpenHPC cluster and have cloned this repository, run
```
bash ./install_osu.sh
```
To install the osu benchmarks (with gnu8+mpich) in your home directory.

Configure the config.yaml to specify the partitions and process layout you want to use for each osu_bw and osu_latency test.
When ready, run
```
python batch_benchmarks.py
```
This will create a number of .slurm files and queue a number of singleton dependent benchmark jobs to run exclusively on nodes.

When all of the jobs finish, you can run the post-processor
```
python post_processor.py
```
which will generate CSV output tables `osu-pt2pt-bw.csv` and `osu-pt2pt-latency.csv`. In these tables, the packet size is measured in
bytes, bandwidth is measured in MB/s, and latency is measured in micro-seconds.


