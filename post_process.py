#!/usr/bin/env python

import os
import subprocess
import shlex
import json
import yaml
import json
from datetime import datetime
import pprint




def load_config( ):

  with open("./config.yaml", 'r') as stream:
    try:
      config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
      print(exc)

  return config

#END load_config()


config = load_config()
results = []
csv_cnt = 0

for bench in config['benchmarks']:
    bres = {}
    bres[bench['name']] = {'n_experiments':config['n_experiments'],
                           'latency_packet_size':[],
                           'bandwidth_packet_size':[],
                           'bandwidth':[],
                           'latency':[]}

    for i in range(config['n_experiments']):

        results_file = config['results_directory']+'/'+bench['name']+'-exp-%d' % (i)+'.txt'
        var = ''
        with open(results_file) as rf:
            line = rf.readline()

            while line:

                line = rf.readline()


                if 'bandwidth' in line:
                    if 'begin' in line:
                        var = 'bandwidth'
                        # Skip next two lines
                        check = rf.readline()
                        if 'end' in check:
                            print('Invalid benchmark : {}-{}'.format(bench['name'],str(i)))
                            continue

                        rf.readline()
                        p_size = []
                        val = [] 
                        continue

                    elif 'end' in line:
                        bres[bench['name']]['bandwidth_packet_size'].append(p_size)
                        bres[bench['name']][var].append(val)
                        var = ''
                        continue

                elif 'latency' in line:
                    if 'begin' in line:
                        var = 'latency'
                        # Skip next two lines
                        check = rf.readline()
                        if 'end' in check:
                            print('Invalid benchmark : {}-{}'.format(bench['name'],str(i)))
                            continue

                        rf.readline()
                        p_size = []
                        val = [] 
                        continue

                    elif 'end' in line:
                        bres[bench['name']]['latency_packet_size'].append(p_size)
                        bres[bench['name']][var].append(val)
                        var = ''
                        continue

                if var :
                    sep_line = line.strip().split(' ')
                    p_size.append(sep_line[0])
                    val.append(sep_line[-1])
 
                       

    # Now create csv
    leader = '{date}, {compiler}, {mpi}, {machine_type}, {nodes}, '.format(date=datetime.now().strftime("%Y-%m-%d %H:%M"),
                                                                           compiler=bench['compiler'],
                                                                           mpi=bench['mpi'],
                                                                           machine_type=bench['machine_type'],
                                                                           nodes=bench['nodes'])  


    f = open( 'osu-pt2pt-bw.csv', 'a' )
    if( csv_cnt == 0 ):
        f.write('date, compiler, mpi, machine_type, n_nodes, packet_size, bandwidth \n')
    for i in range(config['n_experiments']):
        for j in range(len(bres[bench['name']]['bandwidth'][i])):
            packet_size = bres[bench['name']]['bandwidth_packet_size'][i][j]
            value = bres[bench['name']]['bandwidth'][i][j]
            f.write(leader+packet_size+', '+value+'\n')
    f.close()

    f = open( 'osu-pt2pt-latency.csv', 'a' )
    if( csv_cnt == 0 ):
        f.write('date, compiler, mpi, machine_type, n_nodes, packet_size, latency \n')

    for i in range(config['n_experiments']):
        for j in range(len(bres[bench['name']]['latency'][i])):
            packet_size = bres[bench['name']]['latency_packet_size'][i][j]
            value = bres[bench['name']]['latency'][i][j]
            f.write(leader+packet_size+', '+value+'\n')

    f.close()
    csv_cnt+=1


