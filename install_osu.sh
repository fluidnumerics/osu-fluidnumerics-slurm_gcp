#!/bin/bash

cwd=$(pwd)
wget https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-5.6.2.tar.gz
tar -xvzf osu-micro-benchmarks-5.6.2.tar.gz

cd osu-micro-benchmarks-5.6.2/

module purge
module load gnu8
module load mpich
./configure CC=$(which mpicc) CXX=$(which mpic++) --prefix=${HOME}/osu_gnu8_mpich
make
make install

cd $cwd
rm -rf osu-micro-benchmarks-5.6.2*

