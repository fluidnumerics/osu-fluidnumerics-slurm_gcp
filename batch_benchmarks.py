#!/usr/bin/env python

import os
import subprocess
import shlex
import json
import yaml
import json
import datetime




def load_config( ):

  with open("./config.yaml", 'r') as stream:
    try:
      config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
      print(exc)

  return config

#END load_config()


config = load_config()

subprocess.check_call(shlex.split('mkdir -p {}'.format(config['results_directory'])))

k = 0
for bench in config['benchmarks']:
    for i in range(config['n_experiments']):
        batch_file = '''#!/bin/bash
#
#SBATCH --partition={SLURM_PARTITION}
#
#SBATCH --account={SLURM_ACCOUNT}
#
#SBATCH --ntasks={NTASKS}
#
#SBATCH --ntasks-per-node={NTASKS_PER_NODE}
#
#SBATCH --dependency=singleton
#
#SBATCH --time=1:00:00
#
#SBATCH --job-name=osu_bench
#
#SBATCH --exclusive
#
#SBATCH -o {RESULTS_DIR}/{NAME}.out
#
#SBATCH -e {RESULTS_DIR}/{NAME}.err
#

{ENV_HANDLER} purge
{ENV_HANDLER} load {COMPILER}
{ENV_HANDLER} load {MPI}

mpifort -O0 -g mpi_affinity.F90 -o affinity

echo "begin[affinity]" > {RESULTS_DIR}/{NAME}.txt
mpirun ./affinity >> {RESULTS_DIR}/{NAME}.txt
mpirun bash -c "cat /proc/self/status | grep Cpus_allowed_list" >> {RESULTS_DIR}/{NAME}.txt
echo "end[affinity]" >> {RESULTS_DIR}/{NAME}.txt

echo " " >> {RESULTS_DIR}/{NAME}.txt

echo "begin[bandwidth]" >> {RESULTS_DIR}/{NAME}.txt
mpirun {OSU_INSTALL_PATH}/libexec/osu-micro-benchmarks/mpi/pt2pt/osu_bw >> {RESULTS_DIR}/{NAME}.txt
echo "end[bandwidth]" >> {RESULTS_DIR}/{NAME}.txt

echo " " >> {RESULTS_DIR}/{NAME}.txt

echo "begin[latency]" >> {RESULTS_DIR}/{NAME}.txt
mpirun {OSU_INSTALL_PATH}/libexec/osu-micro-benchmarks/mpi/pt2pt/osu_latency >> {RESULTS_DIR}/{NAME}.txt
echo "end[latency]" >> {RESULTS_DIR}/{NAME}.txt

{ENV_HANDLER} unload {MPI}
{ENV_HANDLER} unload {COMPILER}

'''.format( SLURM_PARTITION=config['slurm_partition'],
            SLURM_ACCOUNT=config['slurm_account'],
            NODES=bench['nodes'],
            NTASKS=bench['nmpi_ranks'],
            NTASKS_PER_NODE=bench['ntasks_per_node'],
            RESULTS_DIR=config['results_directory'],
            ENV_HANDLER=bench['env_handler'],
            COMPILER=bench['compiler'],
            MPI=bench['mpi'],
            NAME=bench['name']+'-exp-%d' % (i),
            OSU_INSTALL_PATH=bench['osu_install_path'] )

        f = open('osu_batch_%0.2d.slurm' % (k), 'w')
        f.write(batch_file)
        f.close()

        subprocess.call(shlex.split('/apps/slurm/current/bin/sbatch osu_batch_%0.2d.slurm' % (k)))

        k+=1

